package wantsome.project.DB.dto;

import java.util.Objects;

public class SpecieDTO {

    private final int id;
    private final String specie;

    public SpecieDTO(int id, String specie) {
        this.id = id;
        this.specie = specie;
    }

    public SpecieDTO(String specie) {
        this(-1, specie);
    }

    public int getId() {
        return id;
    }

    public String getSpecie() {
        return specie;
    }

    @Override
    public String toString() {
        return "SpecieDTO{" +
                "id=" + id +
                ", specie='" + specie + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SpecieDTO specieDTO = (SpecieDTO) o;
        return id == specieDTO.id &&
                Objects.equals(specie, specieDTO.specie);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, specie);
    }
}
