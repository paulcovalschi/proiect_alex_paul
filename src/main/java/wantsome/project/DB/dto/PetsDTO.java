package wantsome.project.DB.dto;

public class PetsDTO {

    private final int id;
    private final String nume;
    private final int rasaId;

    private final int microcip;
    private final String culoare;
    private final int proprietarId;
    private final String sex;
    private final double greutate;
    private final double varsta;

    public PetsDTO(int id, String nume, int rasaId, int microcip, String culoare,
                   int proprietarId, String sex, double greutate, double varsta) {
        this.id = id;
        this.rasaId = rasaId;
        this.nume = nume;
        this.microcip = microcip;
        this.culoare = culoare;
        this.proprietarId = proprietarId;
        this.sex = sex;
        this.greutate = greutate;
        this.varsta = varsta;
    }

    public PetsDTO(String nume, int rasaId, int microcip, String culoare,
                   int proprietarId, String sex, double greutate, double varsta) {
        this.id = -1;
        this.rasaId = rasaId;
        this.nume = nume;
        this.microcip = microcip;
        this.culoare = culoare;
        this.proprietarId = proprietarId;
        this.sex = sex;
        this.greutate = greutate;
        this.varsta = varsta;
    }

    public int getId() {
        return id;
    }

    public int getRasaId() {
        return rasaId;
    }

    public String getNume() {
        return nume;
    }

    public int getMicrocip() {
        return microcip;
    }

    public String getCuloare() {
        return culoare;
    }

    public int getProprietarId() {
        return proprietarId;
    }

    public String getSex() {
        return sex;
    }

    public double getGreutate() {
        return greutate;
    }

    public double getVarsta() {
        return varsta;
    }

    @Override
    public String toString() {
        return "PetsDTO{" +
                "id=" + id +
                ", rasaId=" + rasaId +
                ", nume='" + nume + '\'' +
                ", microcip=" + microcip +
                ", culoare='" + culoare + '\'' +
                ", proprietar_id=" + proprietarId +
                ", sex='" + sex + '\'' +
                ", greutate=" + greutate +
                ", varsta=" + varsta +
                '}';
    }
}

