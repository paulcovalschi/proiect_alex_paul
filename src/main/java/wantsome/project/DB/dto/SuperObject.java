package wantsome.project.DB.dto;

import java.util.Date;
import java.util.Objects;

public class SuperObject {
    //proprietar
    private final int id;
    private final String nume;
    private final String prenume;
    private final String adresa;
    private final String oras;
    private final String telefon;
    private final String email;
    //animal
    private final int idAnimal;
    private final String numeAnimal;
    private final int microcip;
    private final String culoare;
    private final String sex;
    private final double greutate;
    private final double varsta;
    //rasa
    private final int idRasa;
    private final String rasa;
    //specie
    private final int idSpecie;
    private final String specie;
    //Examen
    private final int idExamen;
    private final Date date;
    private final String anamneza;
    private final String examen_clinica;
    private final String diagnostic;
    private final String tratament;

    public SuperObject(int id, String nume, String prenume, String adresa, String oras, String telefon, String email,
                       int idAnimal, String numeAnimal, int microcip, String culoare, String sex, double greutate, double varsta, int idRasa,
                       String rasa, int idSpecie, String specie,
                       int idExamen, Date date, String anamneza, String examen_clinica, String diagnostic, String tratament) {
        this.id = id;
        this.nume = nume;
        this.prenume = prenume;
        this.adresa = adresa;
        this.oras = oras;
        this.telefon = telefon;
        this.email = email;
        this.idAnimal = idAnimal;
        this.numeAnimal = numeAnimal;
        this.microcip = microcip;
        this.culoare = culoare;
        this.sex = sex;
        this.greutate = greutate;
        this.varsta = varsta;
        this.idRasa = idRasa;
        this.rasa = rasa;
        this.idSpecie = idSpecie;
        this.specie = specie;
        this.idExamen = idExamen;
        this.date = date;
        this.anamneza = anamneza;
        this.examen_clinica = examen_clinica;
        this.diagnostic = diagnostic;
        this.tratament = tratament;
    }

    public int getId() {
        return id;
    }

    public String getNume() {
        return nume;
    }

    public String getPrenume() {
        return prenume;
    }

    public String getAdresa() {
        return adresa;
    }

    public String getOras() {
        return oras;
    }

    public String getTelefon() {
        return telefon;
    }

    public String getEmail() {
        return email;
    }

    public int getIdAnimal() {
        return idAnimal;
    }

    public String getNumeAnimal() {
        return numeAnimal;
    }

    public int getMicrocip() {
        return microcip;
    }

    public String getCuloare() {
        return culoare;
    }

    public String getSex() {
        return sex;
    }

    public double getGreutate() {
        return greutate;
    }

    public double getVarsta() {
        return varsta;
    }

    public int getIdRasa() {
        return idRasa;
    }

    public String getRasa() {
        return rasa;
    }

    public int getIdSpecie() {
        return idSpecie;
    }

    public String getSpecie() {
        return specie;
    }

    public int getIdExamen() {
        return idExamen;
    }

    public Date getDate() {
        return date;
    }

    public String getAnamneza() {
        return anamneza;
    }

    public String getExamen_clinica() {
        return examen_clinica;
    }

    public String getDiagnostic() {
        return diagnostic;
    }

    public String getTratament() {
        return tratament;
    }

    @Override
    public String toString() {
        return "SuperObject{" +
                "id=" + id +
                ", nume='" + nume + '\'' +
                ", prenume='" + prenume + '\'' +
                ", adresa='" + adresa + '\'' +
                ", oras='" + oras + '\'' +
                ", telefon='" + telefon + '\'' +
                ", email='" + email + '\'' +
                ", idAnimal=" + idAnimal +
                ", numeAnimal=" + numeAnimal +
                ", microcip=" + microcip +
                ", culoare='" + culoare + '\'' +
                ", sex='" + sex + '\'' +
                ", greutate=" + greutate +
                ", varsta=" + varsta +
                ", idRasa=" + idRasa +
                ", rasa='" + rasa + '\'' +
                ", idSpecie=" + idSpecie +
                ", specie='" + specie + '\'' +
                ", idExamen=" + idExamen +
                ", date=" + date +
                ", anamneza='" + anamneza + '\'' +
                ", examen_clinica='" + examen_clinica + '\'' +
                ", diagnostic='" + diagnostic + '\'' +
                ", tratament='" + tratament + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SuperObject that = (SuperObject) o;
        return id == that.id &&
                idAnimal == that.idAnimal &&
                microcip == that.microcip &&
                Double.compare(that.greutate, greutate) == 0 &&
                Double.compare(that.varsta, varsta) == 0 &&
                idRasa == that.idRasa &&
                idSpecie == that.idSpecie &&
                idExamen == that.idExamen &&
                Objects.equals(nume, that.nume) &&
                Objects.equals(prenume, that.prenume) &&
                Objects.equals(adresa, that.adresa) &&
                Objects.equals(oras, that.oras) &&
                Objects.equals(telefon, that.telefon) &&
                Objects.equals(email, that.email) &&
                Objects.equals(numeAnimal, that.numeAnimal) &&
                Objects.equals(culoare, that.culoare) &&
                Objects.equals(sex, that.sex) &&
                Objects.equals(rasa, that.rasa) &&
                Objects.equals(specie, that.specie) &&
                Objects.equals(date, that.date) &&
                Objects.equals(anamneza, that.anamneza) &&
                Objects.equals(examen_clinica, that.examen_clinica) &&
                Objects.equals(diagnostic, that.diagnostic) &&
                Objects.equals(tratament, that.tratament);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nume, prenume, adresa, oras, telefon, email, idAnimal, numeAnimal, microcip, culoare, sex, greutate, varsta, idRasa, rasa, idSpecie, specie, idExamen, date, anamneza, examen_clinica, diagnostic, tratament);
    }
}