package wantsome.project.DB.dto;

import java.util.Date;
import java.util.Objects;

public class ExamenDTO {

    private final int id;
    private final int petId;
    private final Date date;
    private final String anamneza;
    private final String examenClinica;
    private final String diagnostic;
    private final String tratament;

    public ExamenDTO(int id, int petId, Date date, String anamneza, String examenClinica, String diagnostic, String tratament) {
        this.id = id;
        this.petId = petId;
        this.date = date;
        this.anamneza = anamneza;
        this.examenClinica = examenClinica;
        this.diagnostic = diagnostic;
        this.tratament = tratament;
    }

    public ExamenDTO(int petId, Date date, String anamneza, String examenClinica, String diagnostic, String tratament) {
        this(-1, petId, date, anamneza, examenClinica, diagnostic, tratament);
    }

    public int getId() {
        return id;
    }

    public int getPetId() {
        return petId;
    }

    public Date getDate() {
        return date;
    }

    public String getAnamneza() {
        return anamneza;
    }

    public String getExamenClinica() {
        return examenClinica;
    }

    public String getDiagnostic() {
        return diagnostic;
    }

    public String getTratament() {
        return tratament;
    }

    @Override
    public String toString() {
        return "ExamenDTO{" +
                "id=" + id +
                ", pet_id=" + petId +
                ", date=" + date +
                ", anamneza='" + anamneza + '\'' +
                ", examen_clinica='" + examenClinica + '\'' +
                ", diagnostic='" + diagnostic + '\'' +
                ", tratament='" + tratament + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExamenDTO examenDTO = (ExamenDTO) o;
        return id == examenDTO.id &&
                petId == examenDTO.petId &&
                Objects.equals(date, examenDTO.date) &&
                Objects.equals(anamneza, examenDTO.anamneza) &&
                Objects.equals(examenClinica, examenDTO.examenClinica) &&
                Objects.equals(diagnostic, examenDTO.diagnostic) &&
                Objects.equals(tratament, examenDTO.tratament);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, petId, date, anamneza, examenClinica, diagnostic, tratament);
    }
}
