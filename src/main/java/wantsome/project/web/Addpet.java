package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.DB.dto.*;
import wantsome.project.service.*;

import java.sql.Date;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static wantsome.project.web.SparkUtil.render;


public class Addpet {

    public static String showAddForm(Request req, Response res) {

        String id = req.params("id");
        boolean isUpdate = id != null && !id.isEmpty();

        Optional<ProprietarDTO> prop = Optional.empty();
        if (isUpdate) {
            try {
                prop = ProprietariDao.findProprietar(Integer.parseInt(id));
            } catch (Exception e) {
                System.err.println("Error loading fisa with id '" + id + "': " + e.getMessage());
            }
            if (!prop.isPresent()) {
                return "Error: note " + id + " not found!";
            }
        }

        return renderAddUpdateForm(
                prop.map(i -> String.valueOf(i.getId())).orElse(""),
                prop.map(i -> i.getNume()).orElse(""),
                prop.map(i -> i.getPrenume()).orElse(""),
                prop.map(i -> i.getAdresa()).orElse(""),
                prop.map(i -> i.getOras()).orElse(""),
                prop.map(i -> i.getTelefon()).orElse(""),
                prop.map(i -> i.getEmail()).orElse(""));
    }

    private static String renderAddUpdateForm(String id, String nume, String prenume, String adresa,
                                              String oras, String telefon, String email) {
        Map<String, Object> model = new HashMap<>();
        List<RasaDTO> holdRase = RasaDao.getAll();
        List<SpecieDTO> holdSpecie = SpecieDao.getAll();
        model.put("id", id);
        model.put("nume", nume);
        model.put("prenume", prenume);
        model.put("adresa", adresa);
        model.put("oras", oras);
        model.put("telefon", telefon);
        model.put("email", email);
        model.put("rase", holdRase);
        model.put("specii", holdSpecie);
        model.put("isUpdate", id != null && !id.isEmpty());
        return render(model, "addpet.vm");
    }


    public static Object addPet(Request req, Response res) throws SQLException {
        handleAddUpdateRequestPet(req, res);
        handleAddUpdateRequestExamen(req, res);
        return MainPageController.latestTenAdded(req, res);
    }

    private static void handleAddUpdateRequestPet(Request req, Response res) {
        String descriere = req.queryParams("rasa");
        String numeAnimal = req.queryParams("numeAnimal");
        String culoare = req.queryParams("culoare");
        String sex = req.queryParams("sex");
        String greutate = req.queryParams("greutate");
        String varsta = req.queryParams("varsta");
        String microcip = req.queryParams("microcip");
        String nume = req.queryParams("nume");
        String prenume = req.queryParams("prenume");
        String email = req.queryParams("email");
        String proprietarId = req.queryParams("id");

        int rasaId = RasaDao.findId(descriere);
        System.out.println(proprietarId + descriere + numeAnimal + culoare + sex + greutate + varsta + microcip + nume + prenume + email);

        try {
            PetsDTO animal = validateAndBuildPet(rasaId, numeAnimal, microcip, culoare, Integer.parseInt(proprietarId), sex, greutate, varsta);
            PetsDao.insert(animal);
        } catch (Exception e) {
            System.out.println("Error adding new pet");
        }
    }


    private static PetsDTO validateAndBuildPet(int rasaId, String numeAnimal, String microcip, String culoare,
                                               int propId, String sex, String greutate, String varsta) {

        if (numeAnimal == null || numeAnimal.isEmpty()) {
            throw new RuntimeException("Nume is required!");
        }

        if (culoare == null || culoare.isEmpty()) {
            throw new RuntimeException("Culoare is required!");
        }

        if (sex == null || sex.isEmpty()) {
            throw new RuntimeException("Sex is required!");
        }

        if (greutate == null || greutate.isEmpty()) {
            throw new RuntimeException("Greutate is required!");
        }

        if (varsta == null || varsta.isEmpty()) {
            throw new RuntimeException("Varsta is required!");
        }

        return new PetsDTO(numeAnimal, rasaId, Integer.parseInt(microcip), culoare, propId, sex, Double.parseDouble(greutate), Double.parseDouble(varsta));
    }

    private static void handleAddUpdateRequestExamen(Request req, Response res) {
        String data = req.queryParams("data");
        String anamneza = req.queryParams("anamneza");
        String examenClinic = req.queryParams("examen_clinic");
        String diagnostic = req.queryParams("diagnostic");
        String tratament = req.queryParams("tratament");
        String numeAnimal = req.queryParams("numeAnimal");
        String proprietarId = req.queryParams("id");
        int petId = PetsDao.findId(numeAnimal, Integer.parseInt(proprietarId));
        System.out.println(numeAnimal);
        System.out.println("Proprietar id = " + proprietarId);

        try {
            ExamenDTO examen = validateAndBuildExamen(petId, data, anamneza, examenClinic, diagnostic, tratament);
            ExamenDao.insert(examen);
        } catch (Exception e) {
            System.out.println("Error in the examen db");
        }
    }

    private static ExamenDTO validateAndBuildExamen(int petId, String data, String anamneza, String examenClinic,
                                                    String diagnostic, String tratametn) {
        Date selectedDate = null;
        if (data != null && !data.isEmpty()) {
            try {
                selectedDate = Date.valueOf(data);
            } catch (Exception e) {
                throw new RuntimeException("Invalid due date value: '" + data +
                        "', must be a date in format: dd-mm-yyyy");
            }
        }

        if (anamneza == null || anamneza.isEmpty()) {
            throw new RuntimeException("Anamneza is required!");
        }

        if (examenClinic == null || examenClinic.isEmpty()) {
            throw new RuntimeException("Examen Clinic is required!");
        }

        if (diagnostic == null || diagnostic.isEmpty()) {
            throw new RuntimeException("Diagnostic is required!");
        }

        if (tratametn == null || tratametn.isEmpty()) {
            throw new RuntimeException("Tratament is required!");
        }

        return new ExamenDTO(petId, selectedDate, anamneza, examenClinic, diagnostic, tratametn);
    }
}
