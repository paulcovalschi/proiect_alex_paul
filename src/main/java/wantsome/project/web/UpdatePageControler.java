package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.DB.dto.*;
import wantsome.project.service.*;

import java.sql.Date;
import java.util.*;

import static wantsome.project.web.SparkUtil.render;

public class UpdatePageControler {

    public static String showAddUpdateForm(Request req, Response res) {

        String id = req.params("id");
        boolean isUpdate = id != null && !id.isEmpty();

        Optional<SuperObject> sObject = Optional.empty();
        List<ExamenDTO> examene = new ArrayList<>();

        if (isUpdate) {
            try {
                sObject = SuperObjectDao.joinTablesByID(Integer.parseInt(id));
                examene = ExamenDao.getAll(Integer.parseInt(id));
            } catch (Exception e) {
                System.err.println("Error loading fisa with id '" + id + "': " + e.getMessage());
            }
            if (!sObject.isPresent()) {
                return "Error: note " + id + " not found!";
            }
        }

        return renderAddUpdateForm(
                sObject.map(i -> String.valueOf(i.getId())).orElse(""),
                sObject.map(i -> i.getNume()).orElse(""),
                sObject.map(i -> i.getPrenume()).orElse(""),
                sObject.map(i -> i.getAdresa()).orElse(""),
                sObject.map(i -> i.getOras()).orElse(""),
                sObject.map(i -> i.getTelefon()).orElse(""),
                sObject.map(i -> i.getEmail()).orElse(""),
                sObject.map(i -> String.valueOf(i.getIdAnimal())).orElse(""),
                sObject.map(i -> i.getNumeAnimal()).orElse(""),
                sObject.map(i -> String.valueOf(i.getMicrocip())).orElse(""),
                sObject.map(i -> i.getCuloare()).orElse(""),
                sObject.map(i -> i.getSex()).orElse(""),
                sObject.map(i -> String.valueOf(i.getGreutate())).orElse(""),
                sObject.map(i -> String.valueOf(i.getVarsta())).orElse(""),
                sObject.map(i -> String.valueOf(i.getIdRasa())).orElse(""),
                sObject.map(i -> i.getRasa()).orElse(""),
                sObject.map(i -> String.valueOf(i.getIdSpecie())).orElse(""),
                sObject.map(i -> i.getSpecie()).orElse(""),
                sObject.map(i -> String.valueOf(i.getIdExamen())).orElse(""),
                sObject.map(i -> i.getDate() != null ? i.getDate().toString() : "").orElse(""),
                sObject.map(SuperObject::getAnamneza).orElse(""),
                sObject.map(i -> i.getExamen_clinica()).orElse(""),
                sObject.map(SuperObject::getDiagnostic).orElse(""),
                sObject.map(SuperObject::getTratament).orElse(""),
                examene,
                "");
    }

    private static String renderAddUpdateForm(String id, String nume, String prenume, String adresa, String oras, String telefon, String email,
                                              String idAnimal, String numeAnimal, String microcip, String culoare, String sex, String greutate, String varsta, String idRasa,
                                              String rasa, String idSpecie, String specie,
                                              String idExamen, String date, String anamneza, String examen_clinica, String diagnostic, String tratament, List<ExamenDTO> examen, String errorMessage) {
        Map<String, Object> model = new HashMap<>();
        List<RasaDTO> rase = RasaDao.getAll();
        List<SpecieDTO> specii = SpecieDao.getAll();
        model.put("id", id);
        model.put("nume", nume);
        model.put("prenume", prenume);
        model.put("adresa", adresa);
        model.put("oras", oras);
        model.put("telefon", telefon);
        model.put("email", email);
        model.put("idAnimal", idAnimal);
        model.put("numeAnimal", numeAnimal);
        model.put("culoare", culoare);
        model.put("sex", sex);
        model.put("microcip", microcip);
        model.put("greutate", greutate);
        model.put("varsta", varsta);
        model.put("idRasa", idRasa);
        model.put("rasa", rasa);
        model.put("idSpecie", idSpecie);
        model.put("specie", specie);
        model.put("idExamen", idExamen);
        model.put("data", date);
        model.put("anamneza", anamneza);
        model.put("examenClinic", examen_clinica);
        model.put("diagnostic", diagnostic);
        model.put("tratament", tratament);
        model.put("errorMsg", errorMessage);
        model.put("rase", rase);
        model.put("specii", specii);
        model.put("isUpdate", id != null && !id.isEmpty());
        model.put("examene", examen);
        return render(model, "update.vm");
    }

    public static Object doUpdate(Request req, Response res) {

        String id = req.queryParams("id");
        String nume = req.queryParams("nume");
        String prenume = req.queryParams("prenume");
        String adresa = req.queryParams("adresa");
        String oras = req.queryParams("oras");
        String telefon = req.queryParams("telefon");
        String email = req.queryParams("email");

        String idAnimal = req.queryParams("idAnimal");
        String numeAnimal = req.queryParams("numeAnimal");
        String culoare = req.queryParams("culoare");
        String sex = req.queryParams("sex");
        String greutate = req.queryParams("greutate");
        String varsta = req.queryParams("varsta");
        String microcip = req.queryParams("microcip");

        String rasaId = req.queryParams("idRasa");
        String rasa = req.queryParams("rasa");
        String specie = req.queryParams("specie");
        String specieId = req.queryParams("idSpecie");
        int idSpecie = SpecieDao.findId(specie);
        int rasaIdPet = RasaDao.findId(rasa);

        try {
            updateMultipleExamene(req, res);
            ProprietarDTO schimbPro = new ProprietarDTO(Integer.parseInt(id), nume, prenume, adresa, oras, telefon, email);
            ProprietariDao.updateProprietar(schimbPro);
            SpecieDTO spec = new SpecieDTO(Integer.parseInt(specieId), specie);
            SpecieDao.update(spec);
            PetsDTO pet = new PetsDTO(Integer.parseInt(idAnimal), numeAnimal, rasaIdPet, Integer.parseInt(microcip), culoare, Integer.parseInt(id), sex, Double.parseDouble(greutate), Double.parseDouble(varsta));
            PetsDao.update(pet);
            RasaDTO schimbRasa = new RasaDTO(Integer.parseInt(rasaId), rasa, idSpecie);
            RasaDao.update(schimbRasa);
            updateMultipleExamene(req, res);

            return MainPageController.latestTenAdded(req, res);
        } catch (Exception e) {
            List<ExamenDTO> examen = new ArrayList<>();
            return renderAddUpdateForm(id, nume, prenume, adresa, oras, telefon, email, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", examen, "");
        }
    }

    private static void updateMultipleExamene(Request req, Response res) {
        String count = req.queryParams("count");
        for (int i = 1; i <= Integer.parseInt(count); i++) {
            System.out.println(i);
            String data = req.queryParams("data" + i);
            Date newDate = Date.valueOf(data);
            String anamneza = req.queryParams("anamneza" + i);
            String examenClinic = req.queryParams("examen_clinic" + i);
            String diagnostic = req.queryParams("diagnostic" + i);
            String tratament = req.queryParams("tratament" + i);
            String idExamene = req.queryParams("id" + i);
            String idAnimal = req.queryParams("idAnimal");
            System.out.println(idExamene + "" + newDate + "" + anamneza + examenClinic + diagnostic + tratament);
            ExamenDTO schimbExamen = new ExamenDTO(Integer.parseInt(idExamene), Integer.parseInt(idAnimal), newDate, anamneza, examenClinic, diagnostic, tratament);
            ExamenDao.update(schimbExamen);
        }
    }
}
