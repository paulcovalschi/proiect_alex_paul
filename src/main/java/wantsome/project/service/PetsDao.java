package wantsome.project.service;

import wantsome.project.DB.dto.PetsDTO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PetsDao {


    public static List<PetsDTO> getAll() {
        List<PetsDTO> pets = new ArrayList<>();

        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery("select * from pets order by greutate")) {

            while (rs.next()) {

                int id = rs.getInt("id");
                int rasaId = rs.getInt("rasa_id");
                String nume = rs.getString("nume");
                int microcip = rs.getInt("microcip");
                String culoare = rs.getString("culoare");
                int proprietarId = rs.getInt("proprietar_id");
                String sex = rs.getString("sex");
                int greutate = rs.getInt("greutate");
                int age = rs.getInt("varsta");

                PetsDTO pet = new PetsDTO(id, nume, rasaId, microcip, culoare, proprietarId, sex, greutate, age);

                pets.add(pet);
            }
        } catch (SQLException e) {
            System.out.println("Error loading pets " + e.getMessage());
        }
        return pets;
    }


    static PetsDTO extractNoteFromResult(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        int rasaId = rs.getInt("rasa_id");
        String nume = rs.getString("nume");
        int microcip = rs.getInt("microcip");
        String culoare = rs.getString("culoare");
        int proprietarId = 0;
        String sex = rs.getString("sex");
        double greutate = rs.getDouble("greutate");
        double age = rs.getDouble("varsta");
        return new PetsDTO(id, nume, rasaId, microcip, culoare, proprietarId, sex, greutate, age);
    }


    public static void insert(PetsDTO pet) {
        String sql = "insert into pets(nume , rasa_id, microcip, culoare, proprietar_id, sex, greutate,varsta)" +
                "VALUES (?,?,?,?,?,?,?,?);";
        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setString(1, pet.getNume());
            ps.setInt(2, pet.getRasaId());
            ps.setInt(3, pet.getMicrocip());
            ps.setString(4, pet.getCuloare());
            ps.setInt(5, pet.getProprietarId());
            ps.setString(6, pet.getSex());
            ps.setDouble(7, pet.getGreutate());
            ps.setDouble(8, pet.getVarsta());

            ps.execute();

        } catch (SQLException e) {
            System.err.println("Error inserting in db pets " + pet + " : " + e.getMessage());
        }
    }


    public static void update(PetsDTO pet) {

        String sql = "UPDATE pets SET rasa_id =?, nume =?, microcip =?," +
                "culoare =?, proprietar_id =?, sex =?," +
                "greutate =?, varsta =?" +
                "WHERE id =?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setInt(1, pet.getRasaId());
            ps.setString(2, pet.getNume());
            ps.setInt(3, pet.getMicrocip());
            ps.setString(4, pet.getCuloare());
            ps.setInt(5, pet.getProprietarId());
            ps.setString(6, pet.getSex());
            ps.setDouble(7, pet.getGreutate());
            ps.setDouble(8, pet.getVarsta());
            ps.setLong(9, pet.getId());

            ps.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Error while updating pets " + pet + " : " + e.getMessage());
        }
    }

    public static void delete(long id) {

        String sql = "DELETE FROM pets WHERE id = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            ps.execute();

        } catch (SQLException e) {
            System.err.println("Error while deleting pet " + id + ": " + e.getMessage());
        }
    }


    public static int findId(String nume, int proprietarId) {
        int code = 0;
        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery("SELECT id FROM pets \n" +
                     "where nume like \"" + nume + "\" and proprietar_id like\"" + proprietarId + "\" ;")) {

            //maybe not necessary
            while (rs.next()) {
                code = rs.getInt("id");
            }

        } catch (SQLException e) {
            System.err.println("Error finding id " + nume + " : " + e.getMessage());
        }
        return code;
    }

    public static Optional<PetsDTO> findPet(int id) {
        String sql = "SELECT * from pets\n" +
                "where id = ?;";
        {

            try (Connection conn = DbManager.getConnection();
                 PreparedStatement ps = conn.prepareStatement(sql)) {
                //maybe not necessary
                ps.setInt(1, id);
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        PetsDTO pet = PetsDao.extractNoteFromResult(rs);
                        System.out.println(Optional.of(pet));
                        return Optional.of(pet);
                    }
                }
            } catch (SQLException e) {
                System.err.println("Error loading pet  document with id " + id + " : " + e.getMessage());
            }
            return Optional.empty();
        }
    }
}
