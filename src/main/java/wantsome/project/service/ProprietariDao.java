package wantsome.project.service;

import wantsome.project.DB.dto.ProprietarDTO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProprietariDao {

    public static List<ProprietarDTO> getAll() throws SQLException {

        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery("select * from proprietar order by id")) {

            List<ProprietarDTO> proprietari = new ArrayList<>();
            while (rs.next()) {

                int id = rs.getInt("id");
                String nume = rs.getString("nume");
                String prenume = rs.getString("prenume");
                String adresa = rs.getString("adresa");
                String oras = rs.getString("oras");
                String tel = rs.getString("telefon");
                String email = rs.getString("email");

                ProprietarDTO proprietar = new ProprietarDTO(id, nume, prenume, adresa, oras, tel, email);
                proprietari.add(proprietar);
            }
            return proprietari;
        }
    }


    public static void insert(ProprietarDTO proprietar) {
        String sql = "INSERT INTO proprietar" +
                "(" + "nume" + "," + "prenume" + "," + "adresa" + "," + "oras" + "," + "telefon" + "," + "email" + ")" +
                "VALUES(?, ?, ?, ?, ?, ?)";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setString(1, proprietar.getNume());
            ps.setString(2, proprietar.getPrenume());
            ps.setString(3, proprietar.getAdresa());
            ps.setString(4, proprietar.getOras());
            ps.setString(5, proprietar.getTelefon());
            ps.setString(6, proprietar.getEmail());

            ps.execute();

        } catch (SQLException e) {
            System.err.println("Error inserting in db pets " + proprietar + " : " + e.getMessage());
        }
    }


    public static void updateProprietar(ProprietarDTO prop) {
        String sql = "UPDATE proprietar SET nume =?," +
                "prenume =?, adresa =?, oras =?,telefon =?,email =?" +
                "WHERE id =?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setString(1, prop.getNume());
            ps.setString(2, prop.getPrenume());
            ps.setString(3, prop.getAdresa());
            ps.setString(4, prop.getOras());
            ps.setString(5, prop.getTelefon());
            ps.setString(6, prop.getEmail());
            ps.setInt(7, prop.getId());

            ps.executeUpdate();

        } catch (SQLException e) {
            System.err.println("Error while updating proprietar : " + e.getMessage());
        }
    }


    public void delete(long id) {

        String sql = "DELETE FROM  proprietar WHERE id" + " = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            ps.execute();

        } catch (SQLException e) {
            System.err.println("Error while deleting proprietar " + id + ": " + e.getMessage());
        }
    }


    public static int findId(String nume, String prenume, String email) {
        int code = 0;
        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery("SELECT id FROM proprietar\n" +
                     "where nume like \"" + nume + "\" and prenume like\"" + prenume + "\" or email\n" +
                     "like \"" + email + "\";")) {

            //maybe not necessary
            while (rs.next()) {
                code = rs.getInt("id");
            }

        } catch (SQLException e) {
            System.err.println("Error inserting in db pets " + "proprietar" + " : " + e.getMessage());
        }
        return code;

    }

    public static Optional<ProprietarDTO> findProprietar(int id) {
        String sql = "SELECT * from proprietar\n" +
                "where id = ?;";
        {

            try (Connection conn = DbManager.getConnection();
                 PreparedStatement ps = conn.prepareStatement(sql)) {
                //maybe not necessary
                ps.setInt(1, id);
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        ProprietarDTO proprietar = ProprietariDao.extractProprietarFromResult(rs);
                        return Optional.of(proprietar);
                    }
                }
            } catch (SQLException e) {
                System.err.println("Error loading pacient document with id " + id + " : " + e.getMessage());
            }
            return Optional.empty();
        }
    }

    private static ProprietarDTO extractProprietarFromResult(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String nume = rs.getString("nume");
        String prenume = rs.getString("prenume");
        String adresa = rs.getString("adresa");
        String oras = rs.getString("oras");
        String tel = rs.getString("telefon");
        String email = rs.getString("email");
        return new ProprietarDTO(id, nume, prenume, adresa, oras, tel, email);
    }
}