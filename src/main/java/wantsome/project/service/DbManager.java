package wantsome.project.service;

import org.sqlite.SQLiteConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbManager {

    private static final String DB_FILE = "newVet.db";

    //provides connection to the database
    public static Connection getConnection() throws SQLException {

        SQLiteConfig config = new SQLiteConfig();
        config.enforceForeignKeys(true); //enable FK support (disabled by default)
        config.setDateStringFormat("MM/DD/yyyy"); //this also seems important, to avoid some problems with date/time fields..

        return DriverManager.getConnection("jdbc:sqlite:" + DB_FILE, config.toProperties());
    }

}
