package wantsome.project.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class PetDdl {

    public static void createTablePets() throws SQLException {

        String sql = "CREATE TABLE if not exists pets (\n" +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT ,\n" +
                "nume varchar(50),\n" +
                "rasa_id int,\n" +
                "microcip INTEGER UNIQUE,\n" +
                "culoare varchar(50),\n" +
                "proprietar_id int, \n" +
                "sex varchar(20),\n" +
                "greutate double,\n" +
                "varsta double,\n" +
                "FOREIGN KEY (rasa_id) REFERENCES rasa(id),\n" +
                "FOREIGN KEY (proprietar_id) REFERENCES proprietar(id)\n" +
                ")";

        try (Connection conn = DbManager.getConnection()) {
            Statement st = conn.createStatement();
            st.execute(sql);
        }
    }

    public static void dropTablePets() throws SQLException {
        String sql = "DROP TABLE IF EXISTS pets;";

        try (Connection conn = DbManager.getConnection()) {
            Statement st = conn.createStatement();
            st.execute(sql);
        }
    }


    public static void createTableSpecie() throws SQLException {

        String sql = "CREATE table if not exists specie (\n" +
                "id integer NOT NULL PRIMARY KEY AUTOINCREMENT ,\n" +
                "descriere varchar(100) UNIQUE\n" +
                ")";

        try (Connection conn = DbManager.getConnection()) {
            Statement st = conn.createStatement();
            st.execute(sql);
        }
    }

    public static void dropTableSpecie() throws SQLException {
        String sql = "DROP TABLE IF EXISTS specie;";

        try (Connection conn = DbManager.getConnection()) {
            Statement st = conn.createStatement();
            st.execute(sql);
        }
    }


    public static void createTableRase() throws SQLException {

        String sql = "create table if not exists rasa (\n" +
                "id integer NOT NULL PRIMARY KEY,\n" +
                "descriere varchar(100) UNIQUE,\n" +
                "specie_id integer,\n" +
                "FOREIGN KEY (specie_id) REFERENCES specie(id)\n" +
                ");";

        try (Connection conn = DbManager.getConnection()) {
            Statement st = conn.createStatement();
            st.execute(sql);
        }
    }

    public static void dropTableRasa() throws SQLException {
        String sql = "DROP TABLE IF EXISTS rasa;";

        try (Connection conn = DbManager.getConnection()) {
            Statement st = conn.createStatement();
            st.execute(sql);
        }
    }


    public static void createTableExamen() throws SQLException {

        String sql = "CREATE TABLE if not exists examen (\n" +
                "ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "PET_ID INTEGER,\n" +
                "DATA_EXAMEN DATE,\n" +
                "ANAMNEZA VARCHAR,\n" +
                "EXAMEN_CLINIC VARCHAR,\n" +
                "DIAGNOSTIC VARCHAR,\n" +
                "TRATAMENT VARCHAR,\n" +
                "FOREIGN KEY(PET_ID) REFERENCES pets(ID)\n" +
                ")";

        try (Connection conn = DbManager.getConnection()) {
            Statement st = conn.createStatement();
            st.execute(sql);
        }
    }

    public static void dropTableExamen() throws SQLException {
        String sql = "DROP TABLE IF EXISTS examen;";

        try (Connection conn = DbManager.getConnection()) {
            Statement st = conn.createStatement();
            st.execute(sql);
        }
    }


    public static void createTableProprietari() throws SQLException {

        String sql = "CREATE TABLE if not exists proprietar (\n" +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "nume VARCHAR(50) NOT NULL,\n" +
                "prenume VARCHAR(50) NOT NULL,\n" +
                "adresa VARCHAR(100),\n" +
                "oras VARCHAR (30),\n" +
                "telefon VARCHAR NOT NULL,\n" +
                "email VARCHAR (50)\n UNIQUE" +
                ");\n";

        try (Connection conn = DbManager.getConnection()) {
            Statement st = conn.createStatement();
            st.execute(sql);
        }
    }

    public static void dropTableProprietari() throws SQLException {
        String sql = "DROP TABLE IF EXISTS proprietar;";

        try (Connection conn = DbManager.getConnection()) {
            Statement st = conn.createStatement();
            st.execute(sql);
        }
    }

}
