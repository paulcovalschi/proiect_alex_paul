package wantsome.project.service;

import wantsome.project.DB.dto.ExamenDTO;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.Arrays;

public class InserareDate {

    public static void insertIntoPets() {

        String sqls =
                //SPECII
                "insert into specie(descriere) values ('Canidae');" +
                        "insert into specie(descriere) values ('Felidae');" +
                        "insert into specie(descriere) values ('Ursidae');" +
                        "insert into specie(descriere) values ('Mustelidae');" +
                        "insert into specie(descriere) values ('Leporidae');" +
                        "insert into specie(descriere) values ('Psittacidae');" +
                        "insert into specie(descriere) values ('Cricetidae');" +

                        //RASE
                        "insert into rasa(descriere, specie_id) values ('Ciobanesc German', 1);" +
                        "insert into rasa(descriere, specie_id) values ('Doberman', 1);" +
                        "insert into rasa(descriere, specie_id) values ('Boxer', 1);" +
                        "insert into rasa(descriere, specie_id) values ('Shih-tzu', 1);" +
                        "insert into rasa(descriere, specie_id) values ('Bichon Fritze', 1);" +
                        "insert into rasa(descriere, specie_id) values ('Akita Inu', 1);" +
                        "insert into rasa(descriere, specie_id) values ('Beagle', 1);" +
                        "insert into rasa(descriere, specie_id) values ('Cane Corso', 1);" +
                        "insert into rasa(descriere, specie_id) values ('Chow-Chow', 1);" +
                        "insert into rasa(descriere, specie_id) values ('Dog german', 1);" +
                        "insert into rasa(descriere, specie_id) values ('Fox Terrier', 1);" +
                        "insert into rasa(descriere, specie_id) values ('Malamut', 1);" +
                        "insert into rasa(descriere, specie_id) values ('Husky', 1);" +
                        "insert into rasa(descriere, specie_id) values ('Mastiff', 1);" +
                        "insert into rasa(descriere, specie_id) values ('Pit Bull', 1);" +
                        "insert into rasa(descriere, specie_id) values ('Presa Canario', 1);" +

                        "insert into rasa(descriere, specie_id) values ('Albastra De Rusia', 2);" +
                        "insert into rasa(descriere, specie_id) values ('British Shorthair', 2);" +
                        "insert into rasa(descriere, specie_id) values ('American Shorthair', 2);" +
                        "insert into rasa(descriere, specie_id) values ('Persana', 2);" +
                        "insert into rasa(descriere, specie_id) values ('American Bobtail', 2);" +
                        "insert into rasa(descriere, specie_id) values ('Sphynx', 2);" +
                        "insert into rasa(descriere, specie_id) values ('Scottish fold', 2);" +
                        "insert into rasa(descriere, specie_id) values ('Europeana', 2);" +
                        "insert into rasa(descriere, specie_id) values ('Birmaneza', 2);" +
                        "insert into rasa(descriere, specie_id) values ('Angora', 2);" +
                        "insert into rasa(descriere, specie_id) values ('American Curl', 2);" +
                        "insert into rasa(descriere, specie_id) values ('Burmaneza', 2);" +
                        "insert into rasa(descriere, specie_id) values ('Maine coon', 2);" +
                        "insert into rasa(descriere, specie_id) values ('Norvegiana de padure', 2);" +
                        "insert into rasa(descriere, specie_id) values ('Siameza', 2);" +
                        "insert into rasa(descriere, specie_id) values ('Ursul Brun', 3);" +
                        "insert into rasa(descriere, specie_id) values ('Ursul Panda', 3);" +
                        "insert into rasa(descriere, specie_id) values ('Ursul Negru Asiatic', 3);" +
                        "insert into rasa(descriere, specie_id) values ('Ursul Lenes', 3);" +
                        "insert into rasa(descriere, specie_id) values ('Ursul Solar', 3);" +
                        "insert into rasa(descriere, specie_id) values ('Rex', 5);" +
                        "insert into rasa(descriere, specie_id) values ('Marele Alb', 5);" +
                        "insert into rasa(descriere, specie_id) values ('Uriasul Belgian', 5);" +
                        "insert into rasa(descriere, specie_id) values ('Berbec Francez', 5);" +
                        "insert into rasa(descriere, specie_id) values ('Berbec Englez', 5);" +
                        "insert into rasa(descriere, specie_id) values ('Albastru vienez', 5);" +
                        "insert into rasa(descriere, specie_id) values ('Leghorn', 6);" +
                        "insert into rasa(descriere, specie_id) values ('Rhode-Island', 6);" +
                        "insert into rasa(descriere, specie_id) values ('Plymouth-Rock', 6);" +
                        "insert into rasa(descriere, specie_id) values ('Gat-golas de Transilvania', 6);" +
                        "insert into rasa(descriere, specie_id) values ('Brahma', 6);" +
                        "insert into rasa(descriere, specie_id) values ('Cochinchina', 6);" +
                        "insert into rasa(descriere, specie_id) values ('Pitica de portelan', 6);" +
                        "insert into rasa(descriere, specie_id) values ('Olandeza', 6);" +
                        "insert into rasa(descriere, specie_id) values ('Houdan', 6);" +
                        "insert into rasa(descriere, specie_id) values ('Pitic Siberian', 7);" +
                        "insert into rasa(descriere, specie_id) values ('Pitic Rusesc Campbell', 7);" +
                        "insert into rasa(descriere, specie_id) values ('Sirian', 7);" +
                        "insert into rasa(descriere, specie_id) values ('Chinezesc', 7);" +
                        "insert into rasa(descriere, specie_id) values ('Pitic Roborosvski', 7);" +

                        //PROPRIETARI
                        "insert into proprietar(nume, prenume, adresa, oras, telefon, email) values ('Popescu', 'Adina', 'Iasi, Aleea Rozelor 45', 'Iasi', 0724565589, 'adina@yahoo.com');" +
                        "insert into proprietar(nume, prenume, adresa, oras, telefon, email) values ('Costache', 'Adrian', 'Iasi, Aleea Decebal 20', 'Iasi', 0722354659, 'bogdan@yahoo.com');" +
                        "insert into proprietar(nume, prenume, adresa, oras, telefon, email) values ('Cristescu', 'Florin', 'Iasi, B-dul Dacia 123', 'Iasi', 0744858954, 'florin@yahoo.com');" +
                        "insert into proprietar(nume, prenume, adresa, oras, telefon, email) values ('Popescu', 'Albert', 'Iasi, strada Anastasie Panu 20', 'Iasi', 0721542852, 'albert@yahoo.com');" +
                        "insert into proprietar(nume, prenume, adresa, oras, telefon, email) values ('Florescu', 'Ciprian', 'Iasi, strada Tutora 19', 'Iasi', 0766784578, 'ciprian@yahoo.com');" +
                        "insert into proprietar(nume, prenume, adresa, oras, telefon, email) values ('Topescu', 'Alina', 'Iasi, strada Palat 123', 'Iasi', 0740825465, 'alina@yahoo.com');" +
                        "insert into proprietar(nume, prenume, adresa, oras, telefon, email) values ('Dartu', 'Apreotesei', 'Iasi, strada Pacurari 12', 'Iasi', 0724568889, 'sebastian@yahoo.com');" +
                        "insert into proprietar(nume, prenume, adresa, oras, telefon, email) values ('Manolescu', 'George', 'Iasi, strada Bularga 23', 'Iasi', 0740084568, 'valentin@yahoo.com');" +

                        "insert into pets(nume, rasa_id, microcip, culoare, proprietar_id, sex, greutate, varsta) values ('Rex', 1, 198765, 'Negru', 1 , 'Mascul', 35.5, 3);" +
                        "insert into pets(nume, rasa_id, microcip, culoare, proprietar_id, sex, greutate, varsta) values ('Lego', 2, 198735, 'Negru', 1 , 'Mascul', 27, 2.4);" +
                        "insert into pets(nume, rasa_id, microcip, culoare, proprietar_id, sex, greutate, varsta) values ('Whisky', 3, 198456, 'Tigrat', 2 , 'Mascul', 25 , 2.2);" +
                        "insert into pets(nume, rasa_id, microcip, culoare, proprietar_id, sex, greutate, varsta) values ('Bravo', 4, 198321, 'Alb cu negru', 3 , 'Mascul', 9, 3);" +
                        "insert into pets(nume, rasa_id, microcip, culoare, proprietar_id, sex, greutate, varsta) values ('Alpha', 7, 198899, 'Maro cu negru', 4 , 'Mascul', 15, 10);" +
                        "insert into pets(nume, rasa_id, microcip, culoare, proprietar_id, sex, greutate, varsta) values ('Nemo', 5, 198467, 'Maro cu alb', 4 , 'Mascul', 35.5, 5.6);" +
                        "insert into pets(nume, rasa_id, microcip, culoare, proprietar_id, sex, greutate, varsta) values ('Killer', 1, 198908, 'Negru', 5 , 'Mascul', 42, 7.2);" +
                        "insert into pets(nume, rasa_id, microcip, culoare, proprietar_id, sex, greutate, varsta) values ('Xena', 18, 198123, 'Negru', 5 , 'Femela', 3.4, 3);" +
                        "insert into pets(nume, rasa_id, microcip, culoare, proprietar_id, sex, greutate, varsta) values ('Zelda', 21, 198976, 'Gri', 6 , 'Femela', 0.4 , 0.5);" +
                        "insert into pets(nume, rasa_id, microcip, culoare, proprietar_id, sex, greutate, varsta) values ('Cookie', 25, 198583, 'Alba', 6 , 'Mascul', 3.2 , 2.7);" +
                        "insert into pets(nume, rasa_id, microcip, culoare, proprietar_id, sex, greutate, varsta) values ('Vape', 26, 198184, 'Negru cu alb', 6 , 'Mascul', 2.5 , 2.4);" +
                        "insert into pets(nume, rasa_id, microcip, culoare, proprietar_id, sex, greutate, varsta) values ('Hercules', 27, 198177, 'Gri', 7 , 'Mascul', 9, 10);" +
                        "insert into pets(nume, rasa_id, microcip, culoare, proprietar_id, sex, greutate, varsta) values ('Odin', 21, 198146, 'Negru cu alb', 8 , 'Mascul', 6,4.7);" +
                        "insert into pets(nume, rasa_id, microcip, culoare, proprietar_id, sex, greutate, varsta) values ('Thor', 25, 198166, 'Portocaliu', 8 , 'Mascul', 35.5, 4.7);";

        System.out.println("Inserting sample data...");

        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement()) {

            Arrays.stream(sqls.split(";"))
                    .forEach(sql -> {
                        try {
                            st.execute(sql);
                        } catch (Exception e) {
                            System.out.println("error running sql: '" + sql + "' - error: " + e.getMessage());
                        }
                    });

        } catch (SQLException e) {
            System.out.println("Error inserting sample data: " + e.getMessage());
        }

        //insert exams using DAO (important for the date fields)
        ExamenDao.insert(new ExamenDTO(1, date(10, 10, 2018), "scarpinat intens, goluri in blana", "zone de alopecie in zona lombara,mucoasa roz,", "Raie", "Ivermectina"));
        ExamenDao.insert(new ExamenDTO(2, date(23, 12, 2019), "apetitului redus, voma, febra, instabilitate pe membre,", "hipotermie,mucoasa bucala galbena,urina cu sange", "Babesioza", "Imizol"));
        ExamenDao.insert(new ExamenDTO(3, date(2, 2, 2019), "castrare", "Temperatura normala, muscoase roz, greutate normala", "Clinic sanatos", "Castrare"));
        ExamenDao.insert(new ExamenDTO(4, date(3, 1, 2017), "vaccinare anuala", "Temperatura normala, muscoase roz, greutate normala", "clinic sanatos", "DHPPI-R"));
        ExamenDao.insert(new ExamenDTO(5, date(3, 3, 2016), "sterilizare", "Temperatura normala, muscoase roz, greutate normala", "Clinis sanatos", "Ovarohisterectomie"));
        ExamenDao.insert(new ExamenDTO(6, date(12, 12, 2012), "voma, diaree, inapetenta", "hipotermie,mucoasa bucala palida, prezenta de paraziti in scaun", "Giardioza", "Drontal +"));
        ExamenDao.insert(new ExamenDTO(7, date(6, 12, 2019), "apetitului redus, voma, febra, instabilitate pe membre", "hipotermie,mucoasa bucala palida,respiratie greoaie, pete pulmonare,", "Metastaze pulmonare", "Eutanasiere"));
        ExamenDao.insert(new ExamenDTO(8, date(6, 11, 2019), "vaccinare anuala", "Temperatura normala, muscoase roz, greutate normala", "clinic sanatos", "Mixohemovirovac"));
        ExamenDao.insert(new ExamenDTO(9, date(10, 1, 2019), "vaccinare anuala", "Temperatura normala, muscoase roz, greutate normala", "Clinic sanatos", "Felocell"));
        ExamenDao.insert(new ExamenDTO(10, date(19, 9, 2018), "scarpinat intens la nivelul urechii", "in urma examenului microscopic s-au descoperit paraziti auriculari", "Otodectes", "Picaturi auriculare cu albendazol si dexametazon"));
        ExamenDao.insert(new ExamenDTO(11, date(12, 6, 2019), "apetitului redus", "hipertermie,mucoasa bucala congestionata, abces la nivelul urechii", "infectie", "drenarea abcesului,tub de dren,penstrep,tetramycina"));
        ExamenDao.insert(new ExamenDTO(12, date(3, 10, 2019), "mers anormal", "schiopaturi la nivelul membrului anterior", "fractura inchisa a radiusului", "interventie chirurgicala"));
        ExamenDao.insert(new ExamenDTO(13, date(1, 4, 2010), "consult periodic", "Temperatura normala, muscoase roz, greutate normala", "Clinic sanatos", "Schimbarea alimentatie"));
        ExamenDao.insert(new ExamenDTO(14, date(1, 4, 2014), "consult periodic", "Temperatura normala, muscoase roz, greutate normala", "Clinic sanatos", "Schimbarea alimentatie"));

        System.out.println("... sample data inserted!");
    }

    private static java.sql.Date date(int day, int month, int year) {
        return java.sql.Date.valueOf(LocalDate.of(year, month, day));
    }
}
