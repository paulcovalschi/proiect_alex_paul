package wantsome.project.service;

import wantsome.project.DB.dto.SuperObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Optional;

public class SuperObjectDao {

    public static Optional<SuperObject> joinTablesByID(int id) {
        String sql = "SELECT pr.*,p.id as idPet,p.nume as numeAnimal,p.culoare,p.greutate,p.microcip,p.sex,p.varsta,r.id as idRasa,r.descriere, \n" +
                "s.id as idSpecie ,s.descriere as descriereSpecie, e.id as idExamen,e.ANAMNEZA,e.DATA_EXAMEN,e.DIAGNOSTIC,e.EXAMEN_CLINIC,e.TRATAMENT " +
                "from pets p , rasa r , specie s, examen e , proprietar pr\n" +
                "where p.id = ? and pr.id = p.proprietar_id AND p.rasa_id = r.id AND r.specie_id = s.id AND p.id = e.pet_id\n" +
                "order by pr.id; ";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setInt(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    SuperObject sObject = SuperObjectDao.extractNoteFromResult(rs);
                    return Optional.of(sObject);
                }
            }
        } catch (SQLException e) {
            System.err.println("Error loading pacient document with id " + id + " : " + e.getMessage());
        }
        return Optional.empty();
    }

    static SuperObject extractNoteFromResult(ResultSet rs) throws SQLException {
        //proprietar
        int id = rs.getInt("id");
        String nume = rs.getString("nume");
        String prenume = rs.getString("prenume");
        String adresa = rs.getString("adresa");
        String oras = rs.getString("oras");
        String tel = rs.getString("telefon");
        String email = rs.getString("email");
        //
        int idAnimal = rs.getInt("idPet");
        String numeAnimal = rs.getString("numeAnimal");
        int microcip = rs.getInt("microcip");
        String culoare = rs.getString("culoare");
        String sex = rs.getString("sex");
        double greutate = rs.getDouble("greutate");
        double age = rs.getDouble("varsta");
        //
        int idRasa = rs.getInt("idRasa");
        String descriere = rs.getString("descriere");
        //
        int idSpecie = rs.getInt("idSpecie");
        String specie = rs.getString("descriereSpecie");
        //
        int idExamen = rs.getInt("idExamen");
        Date data = rs.getDate("data_examen");
        String anamneza = rs.getString("anamneza");
        String examenClinic = rs.getString("examen_clinic");
        String diagnostic = rs.getString("diagnostic");
        String tratament = rs.getString("tratament");


        return new SuperObject(id, nume, prenume, adresa, oras, tel, email, idAnimal, numeAnimal, microcip, culoare, sex, greutate, age, idRasa, descriere, idSpecie, specie, idExamen, data, anamneza, examenClinic, diagnostic, tratament);

    }

}
